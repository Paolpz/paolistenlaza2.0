
package listasenlazadas2.pkg0;
public class Nodo {
   public int dato;
   public Nodo siguiente;
   
   //Insertar al final
   public Nodo(int d){
       this.dato=d;
       this.siguiente=null;
       
   }
   //Inserta al inicio
   public Nodo(int d, Nodo n){
       dato=d;
       siguiente=n;
   }
}
