package listasenlazadas2.pkg0;
public class Lista {
    protected Nodo inicio,fin;
    
    public Lista(){
        inicio=null;
        fin=null;
    }
    public boolean ListaVacia(){
        if(inicio==null){
            return true;
        }else{
            return false;
        }
    }
    public void agregarInicio(int elemento){
        inicio=new Nodo(elemento,inicio);
        if(fin==null){
        fin=inicio;
    }
    }
    public void agregarFinal(int elemento){
        if(!ListaVacia()){
            fin.siguiente=new Nodo(elemento);
            fin=fin.siguiente;
        }else{
            inicio=fin=new Nodo(elemento);
        }
    }
    public void mostrarLista(){
        Nodo recorrer=inicio;
        while(recorrer!=null){
            System.out.print("["+recorrer.dato+"]===>");
            recorrer=recorrer.siguiente;
            
        }
    }
    public int borrarInicio(){
        int elemento=inicio.dato;
        if(inicio==fin){
            inicio=fin=null;    
        }else{
            inicio=inicio.siguiente;
        }
        return elemento;
    }
    public int borrarFinal(){
        int elemento=fin.dato;
        if(inicio==fin){
            inicio=fin=null;
        }else{
            Nodo temporal=inicio;
            while(temporal.siguiente!=fin){
                temporal=temporal.siguiente;
            }
            fin=temporal;
            fin.siguiente=null;
        }
        return elemento;
    }
}
