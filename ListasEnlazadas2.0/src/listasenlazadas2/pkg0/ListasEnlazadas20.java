package listasenlazadas2.pkg0;
import java.util.Scanner;
public class ListasEnlazadas20 {
public static void main(String[] args) {
        Lista lista=new Lista();
        int opcion=0,x;
        do{
           try{
                Scanner leer= new Scanner(System.in);
                System.out.println("\n=====opiones========\n"
                        + "1.-Añadir al principio\n"
                        + "2.-Añadir al Final\n"
                        + "3.-Mostrar datos\n"
                        + "4.-Eliminar inicio\n"
                        + "5.-Eliminar del final\n"
                        + "6.-Salir\n"
                        + "================");
                
                System.out.println("Elige un numero del 0 al 6");
                opcion=leer.nextInt();
              
                switch (opcion) {
                    
                case 1:
                    try{
                       System.out.println("Agregar un dato al inicio");
                        x=leer.nextInt();
                        lista.agregarInicio(x);
                    }catch(NumberFormatException n){
                         System.out.println("La Lista esta vacia");
                    }
                    break;
                
                case 2:
                   try{
                       System.out.println("Agregar un dato al final");
                        x=leer.nextInt();
                        lista.agregarFinal(x);
                    }catch(NumberFormatException n){
                         System.out.println("La Lista esta vacia");
                    }
                    
                case 3:
                     System.out.println("================");
                    lista.mostrarLista();
                    break;
                case 4:
                    try{
                    x=lista.borrarInicio();
                    System.out.println("Ha borrar el dato del inicio");
                    }catch(NumberFormatException n){
                         System.out.println("La Lista esta vacia");
                    }
                    break;
                case 5:
                    try{
                    x=lista.borrarFinal();
                    System.out.println("Ha borrar el dato del Final");
                    }catch(NumberFormatException n){
                         System.out.println("La Lista esta vacia");
                    }
                    break;
                    
                case 6:
                    opcion = 6;
                    break;
                    
                default:
                  System.out.println("Lista terminada");
                  break;  
                }   
           } catch(Exception e){
               System.out.println("Opcion incorrecta");
           }
        }while(opcion!=6);
    }
    
}

   